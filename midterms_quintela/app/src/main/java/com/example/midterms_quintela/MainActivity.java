package com.example.midterms_quintela;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void eatCookie(View view){
        ImageView bCookie = findViewById(R.id.imageView5);
        bCookie.setImageResource(R.drawable.after_cookie);
        TextView first = findViewById(R.id.im_hungry);
        first.setText("I\'m so full");
    }


}
